# ACMIStreamer

Reads a Tacview.txt.acmi (Note the .txt, not .zip) in the same folder as
the .exe and streams it over a network an ACMI reader (e.g. Tacview or
the CinC app). Note that this app streams as fast as the file can be
read and doesn't delay based on the TimeUpdates.

Useful for testing streaming clients.
